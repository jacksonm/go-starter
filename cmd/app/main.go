package main

import (
	"gitlab.com/jacksonm/go-starter/internal/app"
	"log"

	"github.com/ilyakaznacheev/cleanenv"

	"gitlab.com/jacksonm/go-starter/config"
)

func main() {
	// Configuration
	var cfg config.Config

	err := cleanenv.ReadConfig("./config/config.yml", &cfg)
	if err != nil {
		log.Fatalf("Config error: %s", err)
	}

	app.Run(&cfg)
}
