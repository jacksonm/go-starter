## Notes

Here are some things I discovered while creating this project

### Testing
 * To log to the console during a test use: `testing.T.logF("%s", vat)`
 * To enable logs run tests with the verbose flag: `go test -v ./package/`
