## go-starter

The purpose of this project is two-fold:
 * familiarise myself with modern webapp paradigms in golang
 * provide a quick and easy template to quickly spin up a new go service

## Quick start
Local development:
```sh
# Start redis cache
$ docker-compose up
# Run app
$ ./run-local.sh
```

Integration tests (can be run in CI):
```sh
# Start redis cache
$ docker-compose up
# Run integration tests
$ ./test-local.sh
```

## Project structure
### `cmd/app/main.go`
Configuration and logger initialization. Then the main function "continues" in
`internal/app/app.go`.

### `config`
Configuration. First, `config.yml` is read, then environment variables overwrite the yaml config if they match.
The config structure is in the `config.go`.

### `internal/app`
This is where all the main objects are created.
Dependency injection occurs through the "New ..." constructors (see Dependency Injection).
This separates the business logic from other layers.

Will upgrade to use [wire](https://github.com/google/wire) at some point.

### `internal/controller`
Server handler layer (MVC controllers). This project uses a REST http server powered by the [Gin framework](https://github.com/gin-gonic/gin)
 - Handlers are grouped by responsibility.
 - For each group, its own router structure is created, the methods of which process paths.
 - Services, which control business logic, are injected into the router structure.


#### `internal/controller/http`
REST versioning.
For additional versions, add `http/v2` folder with the same structure.
```go
handler := gin.New()
v1.NewRouter(handler, t)
v2.NewRouter(handler, t)
```

### `internal/entity`
Represents the data model. Can be accessed in any layer.

### `internal/service`
Contains business logic. Methods are grouped by responsibility
Repositories, external apis, rpc, and other logic modules are injected here.

#### `internal/service/repo`
Abstract storage layer.

## Dependency Injection
In order to remove the dependence of business logic on external packages, dependency injection is used.
It will also allow you to do auto-generation of mocks (for example with [mockery](https://github.com/vektra/mockery)) and easily write unit tests.

## Inspiration
This project implements [Clean Architecture](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html) and is based on [Go Clean!](https://github.com/evrone/go-clean-template)
