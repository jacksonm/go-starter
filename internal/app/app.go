package app

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/jacksonm/go-starter/config"
	v1 "gitlab.com/jacksonm/go-starter/internal/controller/http/v1"
	"gitlab.com/jacksonm/go-starter/internal/service"
	"gitlab.com/jacksonm/go-starter/internal/service/repo"
	"gitlab.com/jacksonm/go-starter/pkg/httpserver"
	"gitlab.com/jacksonm/go-starter/pkg/logger"
	"gitlab.com/jacksonm/go-starter/pkg/redis"
)

func Run(cfg *config.Config) {

	// logger
	l := logger.New(cfg.Log.Level)

	// data store
	rdb, err := redis.New(cfg.Redis.URL, redis.Database(cfg.Redis.Database))
	if err != nil {
		l.Fatal("app - Run - redis.New: %w", err)
	}
	defer rdb.Close()

	o := service.New(repo.New(rdb))

	// http server
	handler := gin.Default()
	v1.NewRouter(handler, l, o)
	httpserver.New(handler, httpserver.Port(cfg.HTTP.Port))

	//httpServer := httpserver.New(handler, httpserver.Port(cfg.HTTP.Port))
	//httpserver.Server.Start()
	// Shutdown
	//err = httpServer.Shutdown()
	//if err != nil {
	//	l.Error(fmt.Errorf("app - Run - httpServer.Shutdown: %w", err))
	//}

}
