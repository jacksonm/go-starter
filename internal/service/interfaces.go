package service

import (
	"gitlab.com/jacksonm/go-starter/internal/entity"
)

type (
	IObjectService interface {
		Get(string) (entity.Object, error)
		Set(string, entity.Object) (entity.Object, error)
	}

	IObjectRepo interface {
		Get(string) (entity.Object, error)
		Set(string, entity.Object) (entity.Object, error)
	}
)
