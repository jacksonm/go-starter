package repo

import (
	"fmt"
	"gitlab.com/jacksonm/go-starter/internal/entity"
	redis2 "gitlab.com/jacksonm/go-starter/pkg/redis"
)

type ObjectRepo struct {
	rdb *redis2.RdbClient
}

func New(rdb *redis2.RdbClient) *ObjectRepo {
	return &ObjectRepo{rdb}
}

func (r *ObjectRepo) Get(key string) (entity.Object, error) {
	var o entity.Object

	result, err := r.rdb.Redis.Get(key).Result()
	if err != nil {
		return o, fmt.Errorf("ObjectRepo - Get - r.rdb.Redis.Get(): %w", err)
	}

	o = entity.Object{Data: result}
	return o, err
}

func (r *ObjectRepo) Set(key string, object entity.Object) (entity.Object, error) {
	var o entity.Object

	err := r.rdb.Redis.Set(key, object.Data, r.rdb.CacheDuration).Err()
	if err != nil {
		return o, fmt.Errorf("ObjectRepo - Set - r.rdb.Redis.Set(): %w", err)
	}

	result, err := r.rdb.Redis.Get(key).Result()
	if err != nil {
		return o, fmt.Errorf("ObjectRepo - Set - r.rdb.Redis.Get(): %w", err)
	}

	o = entity.Object{Data: result}
	return o, err
}
