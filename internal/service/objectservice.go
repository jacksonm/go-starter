package service

import (
	"fmt"
	"gitlab.com/jacksonm/go-starter/internal/entity"
)

type ObjectService struct {
	repo IObjectRepo
}

func New(r IObjectRepo) *ObjectService {
	return &ObjectService{
		repo: r,
	}
}

func (us *ObjectService) Get(key string) (entity.Object, error) {
	var o entity.Object
	object, err := us.repo.Get(key)
	if err != nil {
		return o, fmt.Errorf("ObjectService - Get - s.repo.Get(): %w", err)
	}

	o = object
	return o, nil
}

func (us *ObjectService) Set(key string, object entity.Object) (entity.Object, error) {
	var o entity.Object
	object, err := us.repo.Set(key, object)
	if err != nil {
		return o, fmt.Errorf("ObjectService - Set - s.repo.Set(): %w", err)
	}

	o = object
	return o, nil
}
