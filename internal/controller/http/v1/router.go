package v1

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/jacksonm/go-starter/internal/service"
	"gitlab.com/jacksonm/go-starter/pkg/logger"
	"net/http"
)

func NewRouter(handler *gin.Engine, l logger.ILogger, o service.IObjectService) {
	handler.Use(gin.Logger())
	handler.Use(gin.Recovery())

	// Health probe
	handler.GET("/healthz", func(c *gin.Context) { c.Status(http.StatusOK) })

	// Routers
	h := handler.Group("/v1")
	{
		newObjectStoreRoutes(h, l, o)
	}
}
