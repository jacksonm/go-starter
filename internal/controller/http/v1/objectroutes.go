package v1

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.com/jacksonm/go-starter/internal/entity"
	"gitlab.com/jacksonm/go-starter/internal/service"
	"gitlab.com/jacksonm/go-starter/pkg/logger"
	"net/http"
)

type objectRoutes struct {
	l logger.ILogger
	o service.IObjectService
}

func newObjectStoreRoutes(handler *gin.RouterGroup, l logger.ILogger, o service.IObjectService) {
	r := &objectRoutes{l: l, o: o}

	h := handler.Group("/")
	{
		h.GET("get/:key", r.get)
		h.POST("set", r.set)
	}
}

type objectResponse struct {
	Object entity.Object `json:"object"`
}

// @Summary     Get an object
// @Description Get an object
// @ID          get
// @Tags  	    object
// @Accept      json
// @Produce     json
// @Success     200 {object} objectResponse
// @Failure     500 {object} response
// @Router      /get/:id [get]
func (r *objectRoutes) get(c *gin.Context) {
	key := c.Param("key")
	object, err := r.o.Get(key)
	if err != nil {
		r.l.Error(err, "http - v1 - get")
		errorResponse(c, http.StatusNotFound, fmt.Sprintf("no saved object with key: %s", key))

		return
	}

	c.JSON(http.StatusOK, objectResponse{object})
}

type storeObjectRequest struct {
	Key    string `json:"key"`
	Object string `json:"object"`
}

// @Summary     Store an object
// @Description Store an object under the given key
// @ID          set
// @Tags  	    object
// @Accept      json
// @Produce     json
// @Success     200 {object} objectResponse
// @Failure     500 {object} response
// @Router      /set [get]
func (r *objectRoutes) set(c *gin.Context) {
	var request storeObjectRequest
	if err := c.ShouldBindJSON(&request); err != nil {
		r.l.Error(err, "http - v1 - get")
		errorResponse(c, http.StatusBadRequest, "invalid request body")

		return
	}

	object, err := r.o.Set(request.Key, entity.Object{
		Data: request.Object,
	})

	if err != nil {
		r.l.Error(err, "http - v1 - set")
		errorResponse(c, http.StatusInternalServerError, fmt.Sprintf("unable to save object. key: %s object: %s", request.Key, request.Object))

		return
	}

	c.JSON(http.StatusOK, objectResponse{object})
}
