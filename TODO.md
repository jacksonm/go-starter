## TODO
 * [x] refactor to implement clean architecture
 * [ ] fix `Database int 'yaml:"database" env:"DATABASE"'` not reading 0 as valid value
 * [ ] fix `entity.Object` to actually have a useful strucutre. Save data as `[]byte` and deserialize  
 * [ ] add users as a concept (or some unique user identifier to hash with saved object)
 * [ ] research testing frameworks (possible test generators)
 * [ ] rename `redis` package so we dont have to import as `redis2` 
 * [ ] add `cache-duration` as redis option
 * [ ] migrate to wire
