package redis

import (
	"fmt"
	"time"

	"github.com/go-redis/redis"
)

const (
	_DefaultCacheDuration = 6 * time.Hour
	_DefaultDatabase      = 0
)

type RdbClient struct {
	CacheDuration time.Duration
	dataBaseName  int

	Redis *redis.Client
}

func New(url string, opts ...Option) (*RdbClient, error) {
	rdb := &RdbClient{
		CacheDuration: _DefaultCacheDuration,
		dataBaseName:  _DefaultDatabase,
	}

	// Inject custom options
	for _, opt := range opts {
		opt(rdb)
	}

	rdb.Redis = redis.NewClient(&redis.Options{
		Addr:     url,
		Password: "",
		DB:       rdb.dataBaseName,
	})

	pong, err := rdb.Redis.Ping().Result()
	if err != nil {
		panic(fmt.Sprintf("Error init Redis: %v", err))
	}

	fmt.Printf("Successfuly connected to redis store: pong message = {%s}", pong)

	return rdb, nil
}

func (rdb *RdbClient) Close() {
	_ = rdb.Redis.Close()
}
