package redis

type Option func(*RdbClient)

func Database(db int) Option {
	return func(rdb *RdbClient) {
		rdb.dataBaseName = db
	}

}
