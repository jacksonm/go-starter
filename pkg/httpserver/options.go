package httpserver

import "net"

type Option func(*Server)

// Port -.
func Port(port string) Option {
	return func(s *Server) {
		s.Server.Addr = net.JoinHostPort("", port)
	}
}
