package serializer

import (
	"crypto/sha256"
	"encoding/base64"
	"fmt"
	"math/big"

	"github.com/google/uuid"
)

func sha256Imp(input string) []byte {
	algorithm := sha256.New()
	algorithm.Write([]byte(input))
	return algorithm.Sum(nil)
}

func base64Encoded(bytes []byte) string {
	encoder := base64.StdEncoding
	encoded := encoder.EncodeToString(bytes)
	return encoded
}

/*
 * creates a key based on the user id and object saved
 * 1. hash object and user id to prevent clashing
 * 2. derive an integer from the hash bytes
 * 3. apply base64 on derived integer and pick the first X characters
 */
func GenerateKey(object string, userId uuid.UUID) string {
	hash := sha256Imp(object + userId.String())
	generatedInt := new(big.Int).SetBytes(hash).Uint64()
	finalKey := base64Encoded([]byte(fmt.Sprintf("%d", generatedInt)))
	return finalKey[:8]
}
