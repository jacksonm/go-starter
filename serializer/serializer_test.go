package serializer

import (
	. "testing"

	"github.com/google/uuid"
	. "github.com/onsi/gomega"
)

const UserIdStr = "e0dba740-fc4b-4977-872c-d360239e6b1a"

func TestKeyGeneration(t *T) {
	g := NewGomegaWithT(t)
	UserId, _ := uuid.Parse(UserIdStr)

	object1 := `{"some":"object"}`
	key1 := GenerateKey(object1, UserId)

	object2 := `{"some":"other_object"}`
	key2 := GenerateKey(object2, UserId)

	object3 := `{"some":"other_other_object"}`
	key3 := GenerateKey(object3, UserId)

	g.Expect(key1).Should(Equal("MTEwNDU3"))
	g.Expect(key2).Should(Equal("NjUyNjMx"))
	g.Expect(key3).Should(Equal("NzI3OTAx"))
}
